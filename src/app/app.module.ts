import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { getAuth, provideAuth } from '@angular/fire/auth';
import { getFirestore, provideFirestore } from '@angular/fire/firestore';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    provideFirebaseApp(() =>
      initializeApp({
        projectId: 'ejercicio-tema17-deploy',
        appId: '1:920933970182:web:9fcc6cdb383ee3b2b66e87',
        storageBucket: 'ejercicio-tema17-deploy.appspot.com',
        apiKey: 'AIzaSyALVuWjDCEMvpH3vmVSNnJXDezpxzW1Ym4',
        authDomain: 'ejercicio-tema17-deploy.firebaseapp.com',
        messagingSenderId: '920933970182',
        measurementId: 'G-RSDVKGWVEY',
      })
    ),
    provideAuth(() => getAuth()),
    provideFirestore(() => getFirestore()),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
